## The Xenin Framework
A framework developed for Garry's Mod, intended to make mundane tasks just a bit easier and more interconnected.

### Major features
* Easy in-game configurator
* SQL query builder & migrations
* MySQL/SQLite support, using MySQLite.
* Tons of pre-built UI panels that follows a specific theme
* Ability to connect scripts together using the common framework interface
* Lots of helper functions

### How to use
If you want to just install it, navigate to [releases](https://gitlab.com/sleeppyy/xeninui/-/releases) and download the newest version.

If you are a developer, you will need to make use of [LAUX](https://gitlab.com/sleeppyy/laux) as this project is not written in Lua, but a superset similar to Lua.

### Docs
Documentation is largely unfinished, but can be found at [https://sleeppyy.gitlab.io/xenin-framework/#/](https://sleeppyy.gitlab.io/xenin-framework/#/)

Big thanks to [Milhane.](https://gitlab.com/chmilhane) for creating large parts of the documentation + the website theme
