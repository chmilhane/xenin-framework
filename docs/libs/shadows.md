# Shadows <client>Client</client>
Also known as Blues Shadows.
Made by [CODE-BLUE](https://www.youtube.com/channel/UCFpuE-Qjn4EWqX-VJ_l7pbw).

## BeginShadow
Call this to begin drawing a shadow.
```laux
BSHADOWS.BeginShadow()
```

## EndShadow
This will draw the shadow, and mirror any other draw calls the happened during drawing the shadow.
```laux
BSHADOWS.EndShadow(intensity: number, spread: number, blur: number, opacity: number, direction: number, distance: number, _shadowOnly: boolean = false)
```
**Argumets**
1. Intensity
2. Spread
3. Blur Size
4. **Optional** Opacity
5. **Optional** Direction
6. **Optional** Distance
7. **Optional** Shadow Only

## Example
```laux
-- Put this code into a Paint function:

local x, y = self:LocalToScreen()

BSHADOWS.BeginShadow()
  -- Draw here
  draw.RoundedBox(6, x, y, w, h, XeninUI.Theme.Background)
BSHADOWS.EndShadow(1, 2, 2, 255, 0, 0)
```