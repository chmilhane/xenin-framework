# XeninUI.Avatar <client>Client</client>
The `XeninUI.Avatar` is used to show a player's Steam avatar, but rounded.  

# Parent
Derives from [Panel](https://wiki.facepunch.com/gmod/Panel).

<!--
# Methods

## SetPlayer
Used to load an avatar for given player.
```laux
XeninUI.Avatar:SetPlayer(ply: Player, size: number)
```
**Arguments**
1. The player to use avatar of.
2. The size of the avatar to use. Acceptable sizes are 32, 64, 184.

---

## SetSteamID
Used to load an avatar by its 64-bit Steam ID (community ID).
```laux
XeninUI.Avatar:SetSteamID(sid64: string, size: number)
```
**Arguments**
1. The 64bit SteamID of the player to load avatar of.
2. The size of the avatar to use. Acceptable sizes are 32, 64, 184.
-->

# Example
```laux
local frame = vgui.Create("XeninUI.Frame")
frame:SetSize(300, 300)
frame:Center()
frame:MakePopup()
frame:SetTitle("XeninUI.Avatar")

local size = 128
local avatar = frame:Add("XeninUI.Avatar")
avatar:SetPlayer(LocalPlayer(), size)
avatar:SetSteamID("76561198202328247", size) -- Sleeppy's SteamID
avatar:SetVertices(size)
avatar:Dock(FILL)
avatar:DockMargin(40, 40, 40, 40)
```
Final Result  
![Output](https://i.imgur.com/nPZAxkI.png)