# Configuration
All these settings can be found at `xeninui/settings/settings.laux`.

## Branding
Should branding be enabled on every frame?
```laux
XeninUI.Branding = false -- Material("xenin/pantheon.png", "smooth")
```

## Materials
```laux
XeninUI.Materials = {
	CloseButton = Material("xenin/closebutton.png", "noclamp smooth"),
	Search = Material("xenin/search.png", "noclamp smooth"),
  Tick = Material("xenin/tick.png", "smooth")
}
```

## Transition Time
Transition Time for UI animations.
```laux
XeninUI.TransitionTime = 0.15
```

## Disable Notification
Should disable XeninUI notification? (True to disable)
```laux
XeninUI.DisableNotification = false
```

## UI Theme
```laux
XeninUI.Theme = {
	Primary = Color(48, 48, 48),
	Navbar = Color(41, 41, 41),
	Background = Color(30, 30, 30),
	Accent = Color(41, 128, 185),
	OrangeRed = Color(228, 104, 78),
	Red = Color(230, 58, 64),
	Green = Color(46, 204, 113),
	Blue = Color(41, 128, 185),
	Yellow = Color(201, 176, 15),
	Purple = Color(142, 68, 173),
	Orange = Color(230, 153, 58),
	LightYellow = Color(189, 201, 15),
	GreenDark = Color(39, 174, 96)
}
```

## Default Sizes
```laux
XeninUI.Frame = {
	Width = 960,
	Height = 720
}
```